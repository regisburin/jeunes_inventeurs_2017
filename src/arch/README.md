# Congrès des Jeunes chercheurs 2017

*Thème: "Transport et déplacements"*

Élèves de l'école St Ignace (64270 CARESSE), Délphine DURCOS, Alex HAMON (pour [La Fab'Brique](http://lafabbrique.org))

Licence: [CC-0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) (Domaine Public)

> ## *"Comment transférer du blé d'une remorque dans un silo"*

Défi "classe" pour le temps d'exposition du congrès.

### Infos

  * Organisation: 
    * *?*
    * Lien Framadate: *?*
  * Classes: ce2/cm1/cm2 (18 élèves)
  * Presentation:
    * **jeudi 18/05/17**
    * Lieu: Salies-de-Béarn (64270)
  * Ateliers:
    * 17/03 - j
    * 31/03 - 1/2 j
    * 14/04 - j
    * (27/04 - j)
  
## Projet

### Croquis/Plans
#### [Croquis de principe](./convoyeur.png)
![Croquis du convoyeur](./convoyeur.png)

#### [Maquette 3D](./)
...

### [Synoptique](./synoptique.pdf)

> *Désigne une présentation, en général graphique, qui permet de saisir d'un seul coup d'œil un ensemble d'informations liées ou un système complexe. L'adjectif synoptique évoque l'idée de « voir en un même ensemble ».<br/>(https://fr.wikipedia.org/wiki/Synoptique)*

  * [Synoptique simplifié](./synoptique_1.png)
  * [Synoptique complet](./synoptique_2.png)


### Matériel

#### Ressources matérielles:
  * récupération (démontage de vieux jouets motorisés/radiocommandés récupérés à la déchetterie de Salies)
  * prêt de matériel électronique supplémentaire (carte de développement Arduino, composants et accessoires), par le biais de La Fab'Brique (FabLab associatif de Salies).

#### Nomenclature
> *Une nomenclature est un tableau contenant la désignation de toutes les pièces qui composent un assemblage d'un dessin industriel.<br/>(https://fr.wikipedia.org/wiki/Nomenclature#Univers_de_la_production_et_de_la_logistique)*

| Ensemble |  Sous-ensemble  |  Désignation  |  Dimensions  |  Qté  |
|----------|-----------------|---------------|:------------:|:-----:|
|  Support  |    |    |    |    |
|    |  Plaque support  |  Bois (contreplaqué, mdf...)  |    |  1  |
|    |  Sol  |  //  |    |  1  |
|    |  Plaque avant  |  //  |    |  1  |
|    |  Plaque arrière  |  //  |    |  1  |
|    |  Pupitre  |  //  |    |  1  |
|    |  Cotés  |  //  |    |  2  |
|  Tapis  |    |    |    |    |
|    |  Axes  |    |    |  2  |
|    |  Rouleaux  |  Bouchons de liège  |    |  2  |
|    |  Tapis  |  Chambre à air |    |  1  |
|    |  Crampons  |  allumettes/batons de sucettes  |    |  ?  |
|    |  Rebord  |  Bois/carton  |    |  1  |
|  Silo  |    |    |    |    |
|    |  Silo  |  Bouteille de soda/lait  |    |  1  |
|    |  Trappe  |  ?  |    |  1  |
|  Tracteur  |    |    |    |    |
|    |  [Tracteur et remorque](./tracteur.png)  |  Carton  |  420x297 (A3) |  1  |
|    |    |    |    |   |


#### Outils
  * [x] Tournevis (cruciformes, plats)
  * [ ] Pince coupante
  * [x] Fer à souder
  * [ ] Cutter/Ciseaux
  * [ ] Colle à papier
  * [x] Colle à bois
  * [x] Pistolet à colle
  * [ ] Scotch à papier (de masquage)

### Mise en oeuvre

  * 17/03
    * Présentation
    * Brainstorming *(=> cahier des charges)*
    * Synoptique basique (=> [synoptique_1](./synoptique_1.png))
    * Liste des éléments à fabriquer
    * Développement à plat du [tracteur](./tracteur.png)
  * 14/04
    * Démontage jouets (=> Découverte mécanique, électrotechnique)
    * Fabrication en ateliers (Silo, Tracteur/remorque, Tapis, Structure)
    * Test moteurs, servos (=> Découverte éléctronique, microcontrôleur *Arduino*)
    * Présentation du synoptique complet (=> [synoptique_2](./synoptique_2.png))
    * Présentation du programme (=> Découverte programmation informatique)
    * Assemblage (=> Soudure ?)

### Programme
  * [Logigramme](./logigramme.pdf)

> *Un organigramme de programmation (parfois appelé algorigramme, logigramme ou plus rarement ordinogramme) est une représentation graphique normalisée de l'enchaînement des opérations et des décisions effectuées par un programme d'ordinateur.<br/>(https://fr.wikipedia.org/wiki/Organigramme_de_programmation)*

  * Basiques: http://troumad.developpez.com/C/algorigrammes/)
  * [Arduino](./)
