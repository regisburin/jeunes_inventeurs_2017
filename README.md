# Congrès des Jeunes chercheurs 2017

*Thème: "Transport et déplacements"*

Élèves de l'école St Ignace (64270 CARESSE), Délphine DURCOS, Alex HAMON (pour [La Fab'Brique](http://lafabbrique.org))

Licence: [CC-0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr) (Domaine Public)

> ## *"Comment transférer du blé d'une remorque dans un silo ?"*

Défi "classe" pour le temps d'exposition du congrès.

### Infos

  * Organisation: 
    * *?*
    * Lien Framadate: *?*
  * Classes: ce1/ce2/cm1/cm2 (18 élèves)
  * Presentation:
    * **jeudi 18/05/17**
    * Lieu: Salies-de-Béarn (64270)
    * Déroulé:
      * Scéance plénière (défi commun à toutes les classes)
      * Expositions (défis de classes)
      * Rencontre/échanges avec deux chercheurs
  * Ateliers en classe:
    * 17/03 - j
    * 31/03 - 1/2 j
    * 14/04 - j
    * (27/04 - j)
  
## Projet

### Idées (groupes de recherches)
  * Tapis roulant
  * Aspiration, ventilation
  * Cuve enterrée
  * Machoire
  * Lever la remorque
  * 

### Matériel

#### Ressources matérielles
  * récupération
  * Matériel de l'école

#### Outillage
  * Tournevis (cruciformes, plats)
  * Pince coupante
  * Cutter/Ciseaux
  * Colle à papier
  * Colle à bois
  * Pistolet à colle
  * Scotch à papier (de masquage)

### Comptes-rendus

  * 17/03
    * Présentation
    * Définitions et précisions sur l'intitulé du défi
    * "Comment ferais-tu pour relever le défi suivant...?" (descriptions/dessins, individuel)
    * Remontée d'idée et débats (collectif)
    * Synthèse (idées retenues)
    * ----
    * Ateliers de construction
    * Présentations
  * 31/03 (prévisionnel)
    * 
  * 14/04 (prévisionnel)
    * 
